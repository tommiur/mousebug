//
//  AppDelegate.swift
//  MouseBugXCode8
//
//  Created by Tommi Urtti on 18.8.2017.
//  Copyright © 2017 Tommi Urtti. All rights reserved.
//


import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
    
    
}
