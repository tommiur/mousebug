//
//  GameScene.swift
//  MouseBugXCode8
//
//  Created by Tommi Urtti on 18.8.2017.
//  Copyright © 2017 Tommi Urtti. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var rightCount = 0
    var leftCount = 0
    
    override func mouseDown(with event: NSEvent) {
        leftCount += 1
    }
    
    override func mouseUp(with event: NSEvent) {
        leftCount -= 1
    }
    
    override func rightMouseDown(with event: NSEvent) {
        rightCount += 1
    }
    
    override func rightMouseUp(with event: NSEvent) {
        rightCount -= 1
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if let counterLabel = childNode(withName: "counterLabel") as? SKLabelNode {
            counterLabel.text = "Left: \(leftCount) Right: \(rightCount)"
        }
    }
}
