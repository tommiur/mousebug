# README #
This project demonstrates lost mouse events when Logitech Marathon M705 mouse is used in macOS Sierra. The counters start building up even though they should stay at zero since mouseDown is always matched by mouseUp.
 
Bug not reproducible with Magic Mouse 2.
